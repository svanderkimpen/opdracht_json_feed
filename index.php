<?php
// header("Content-Type: text/javascript; charset=utf-8");
header('Content-type: application/json');
?>
"meta":{
"title":"My blog",
"url" : "http://localhost/blog"
},
<?php
$dbuser = "root";
$dbpass = "root@admin";
$dbhost = "localhost";
$dbname = "blog";
$sql = "SELECT * FROM `posts` where `postID` > 1 ORDER BY `pubdate` DESC LIMIT 0, 5" ;
try {
$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass); 
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$stmt = $dbh->query($sql); 
$results = $stmt->fetchAll(PDO::FETCH_OBJ);
$dbh = null;
echo '{"posts":'. json_encode($results) .'};';
} catch(PDOException $e) {
echo '{"error":{"text":'. $e->getMessage() .'}}'; 
}